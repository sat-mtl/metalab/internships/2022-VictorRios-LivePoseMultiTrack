---
title:
- Internship Presentation / Présentation de stage
author:
- Firstname Lastname
institute:
- Société des Arts Technologiques [SAT] - metalab
theme:
- default
date:
- 2022/01/10
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: presentation.css
---

## Sommaire / Outline

## Conclusion
