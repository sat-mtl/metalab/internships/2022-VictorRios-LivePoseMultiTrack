# Integration of action and pose estimation and tracking for multiperson videos

* Intern: Victor Rios
* Supervisor(s): Emmanuel Durand & Christian Frisson

## Description

<!-- link post from https://sat-metalab.gitlab.io/lab/categories/internships/ -->

<!-- copy/paste Objective and Tasks from post below, then later transform into Abstract-->
[LivePose](https://gitlab.com/sat-mtl/tools/livepose) is an open-source human skeleton and face keypoints detection software. There are many state-of-the-art pose estimation approaches integrated into Livepose.

Often during immersive experiences, the participant moves around the scenes, resulting in frequent occlusions in given timestamps, while fully visible in the neighboring frames. This project aims to combine pose estimation and tracking for improved pose estimation of immersive media.

Our proposed method is inspired by the below studies in this field:

- [Self-supervised Keypoint Correspondences for Multi-Person Pose Estimation and Tracking in Videos](https://arxiv.org/pdf/2004.12652.pdf)
- [Combining detection and tracking for human pose estimation in videos](https://arxiv.org/pdf/2003.13743.pdf)

### Objectives / Objectifs

- [ ] Complete implementation of one of the papers above (or of similar work appearing or found by the beginning of this project)
- [ ] Partial representation of results by training and testing on one of the publicly available pose datasets
- [ ] (Bonus) Adaptation components for a custom dataset (e.g., custom dataloader, etc)

### Tasks / Tâches

 * MMtracking implementation into Livepose
 * MMAction2 training

## Contributions

<!-- List hyperlinks to main contributions: such as documentation and software repositories -->

* ...

## Documents

<!-- Some of these documents may be optional, as defined by each internship -->

* [Presentation / Présentation](presentation.md) 
* [Report / Rapport](report.md)

## Media

* Please use media file formats defined in [.gitattributes](.gitattributes) or update the list with new formats before versioning your media files.
* Please version your media files in folder [media](media), ideally in a sub-folder with the date of versioning in `yyyy-mm-dd` format, for example `media/2022-03-01/`.

## Feedback

Please note that interns and mentors can provide feedback (for instance for mid-term reviews) using our [gitlab issue template](.gitlab/issue_templates/feedback.md) by [following this guide](https://docs.gitlab.com/ee/user/project/description_templates.html#use-the-templates).
