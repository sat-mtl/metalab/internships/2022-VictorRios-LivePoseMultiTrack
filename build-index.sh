#!/bin/bash

mkdir -p build

cp report.css build/

pandoc -s -f gfm -t html --css report.css ./README.md -o build/index.html

for f in presentation report; do
  sed -i "s/$f.md/$f.html/" build/index.html
done