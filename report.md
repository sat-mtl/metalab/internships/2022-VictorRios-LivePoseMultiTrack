---
title: Synthèse de littérature des méthodes de reconnaissance d'actions en computer vision
author:
- Victor Rios
- Christian Frisson / Emmanuel Durand
institute:
- Société des Arts Technologiques [SAT] - metalab
bibliography:
- biblio.bib
- metalab/bib/metalab-papers.bib 
- metalab/bib/metalab-presentations.bib
---

## Abstract


1) **Mise en situation**

Dans cette revue de littérature, je vais vous présenter l’ensemble des méthodes et des techniques utilisés en vision par ordinateur associé à l’apprentissage profond. C’est un sujet particulièrement en vogue en ce moment dans le monde du Deep Learning appliqué à l’informatique, l’ingénierie, la physique, la biologie… Nous nous focusserons par la suite sur les modèles de reconnaissance d’actions que j‘utiliserai dans mon projet. Premièrement, j’aimerais définir historiquement la computer vision par  “the  construction  of  explicit,  meaningful  descriptions  of  physical objects from images” (Ballard & Brown, 1982). Ainsi, si l'intelligence artificielle permet aux ordinateurs de penser, la vision par ordinateur leur permet de voir, d'observer et de comprendre.

**Domaine d’application**

Il existe néanmoins de nombreux domaines d’applications à la vision par ordinateur tel que :

- La robotique
- Le divertissement
- La sécurité
- La biométrie
- Les voitures intelligentes

Le domaine dans lequel notre projet s’inscrit est l’art numérique. En effet, l'installation interactive en arts numériques est un domaine en constante évolution.  Les  artistes  et  les  créateurs  cherchent  à  utiliser  des technologies plus avancées pour créer des expériences plus immersives pour les visiteurs.

**Problématique abordée**

Cependant, les solutions actuelles de reconnaissance d'action sont souvent coûteuses et difficiles à mettre en place. Il existe donc un besoin de développer des solutions de reconnaissance d'action plus abordables et fiables pour les installations interactives en arts numériques.

La reconnaissance d'action, Human Action Recognition (HAR), est un domaine en croissance de la  reconnaissance de mouvement par les systèmes  d'intelligence  artificielle  (IA).  Il  est  important  pour  de nombreuses applications, notamment la surveillance vidéo, la robotique, la réalité virtuelle, et la génération automatique de sous-titres. 

**Objectifs du projet**

MEGACETA est un projet de collaboration entre la Société des Arts Technologiques de Montréal (SAT) et le studio CREO. Il a pour but de développer une solution de reconnaissance d'action basée sur l'IA pour les installations interactives en arts numériques. Le but de ce projet est d’améliorer cette reconnaissance d’action en implantant un modèle de réseau de neurones capable de reconnaître des actions clés en temps réel. Cette implémentation sera alors intégrée aux deux installations présentes à Montréal et à Paris à l’Aquarium Tropicale du palais de la Porte-Dorée. 

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.001.png)

*Figure 1 : Installation MEGACETA à l’Aquarium Tropicale de Paris (BaronMag, 2021)*

Notre objectif pour ce projet est d’utiliser un modèle en transfert learning sur notre base de données afin de reconnaitre des mouvements en temps réel et transmettre l’information de mouvement à l’installation interactive. Dans l’installation MEGACETA, un certain mouvement reconnu permet d’animer des baleines numériques et leur ordonner différentes actions. Enfin, une fois le modèle entraîné, il devra être intégrer au logiciel de vision de la SAT : LivePose.

**Structure du document**

Notre  synthèse  sera  articulé  autour  de  différents  axes. Premièrement, nous ferons une introduction à l’ensemble des techniques de vision par ordinateur et sur les réseaux de neurones convolutifs pour parfaire notre connaissance du domaine. 

Dans une première partie, nous étudierons l’ensemble des approches en reconnaissance d’action et de mouvement, en expliquant l’ensemble des méthodes. Dans cette partie nous nous étudierons en détail le modèle que nous allons utiliser.

De plus, dans une seconde partie on pourra analyser et critiquer les avantages et inconvénients de ces approches afin de choisir la plus approprié pour notre application. On proposera également une étude analytique des mesures des performances du modèle.

Enfin, on pourra conclure sur les recommandations pour la suite du projet et émettre les premières hypothèses quant à la phase expérimentale.

**Base de données :** 

Un défi assez délicat dans mon projet est la création en collaboration de la dataset d’entrainement et de validation. En effet, nous devrons capturer nous-même ces vidéos dans le studio de Creo à Montréal et recréer un environnement similaire à la SAT pour notre phase de test. On doit alors reproduire l’environnement représenté Figure 1 pour conserver la bonne distribution de la dataset. 

Ainsi, nous avons pour l’heure effectuer deux captations dans les locaux de Créo, ainsi qu’une captation dans notre environnement reproduit à la SAT.  J’envisage  également  d’augmenter  la  dataset  pour  parfaire l’entrainement et la validation du modèle.

**Introduction aux réseaux de neurones convolutifs (CNN) :** 

Une première approche en vision par ordinateur serait d’utiliser un algorithme de Machine Learning « classique », comme la régression logistique ou bien une forêt aléatoire. Bien que ces approches obtiennent des résultats relativement corrects, ce type d’algorithmes ne pourront pas se généraliser aux images dont l’item se retrouverait dans un coin de l’image plutôt qu’au centre de celle-ci. En d’autres termes, le caractère spatial des éléments caractéristiques de certaines catégories n’est pas pris en compte.

Ainsi, on a besoin d’utiliser un algorithme capable de détecter des formes relatives indépendamment de leur position dans l’image : c’est ce que permettent  les  Convolutionnal  Neural  Networks  (CNN).  En  effet,  les algorithmes de vision par ordinateur modernes sont basés sur des réseaux de neurones convolutifs, qui offrent une amélioration importante des performances  par  rapport  aux  algorithmes  de  traitement  d'image traditionnels. 

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.002.png)

*Figure 2 : Réseaux de neurones convolutifs (Bibhu Pala, 2018) Un CNN classique alterne majoritairement entre deux types de couches :*

- Couches avec filtre convolutif + activation (ReLu)  : on réalise un produit entre un filtre et l’input de la couche. La multiplication de ces couches au sein du réseau va permettre d‘extraire des features de plus en plus complexes qui permettront enfin de prédire une classe d’appartenance pour l’item présent dans l’image. On utilise de plus une couche d’activation pour atténuer les valeurs négatives en conservant les valeurs positives, pour améliorer l’efficacité et la rapidité de l’entrainement.
- Couches avec pooling  : elles permettent un undersampling qui va compresser  la  dimension  de  l’image  et  réduire  le  coût computationnel des couches suivantes. En général on utilise une fonction maximum ou moyenne.
- Les  dernières  couches  aplatissent  les  features  via  une  couche Flatten avant d’enchaîner avec des couches dense (FC pour Fully Connected). La dernière couche applique une fonction softmax, afin de déterminer la classe de l’image parmi les dix catégories.

Ainsi, d’après un benchmark effectué par l’entreprise Aquila sur le dataset Fashion MNIST, on peut observer un boost effectif en termes de performance prédictives sur les réseaux de neurones profonds, par rapport aux algorithmes de Machine Learning classique.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.003.png)

*Figure 3 : Résultats des prédictions sur Fashion MNIST en fonction du modèle Aquila, 2016)*

2) **Introduction à la computer vision**

Premièrement, on s’attachera à identifier quelques tâches clés en vision par ordinateur quand on s’intéresse à la détection, la localisation et la classification d’image. Celles-ci témoignent des 6 techniques majeures en computer vision dont je souhaite vous faire part :

- **La classification d’image** consiste à prédire la classe d’un objet dans une image. Plus précisément, elle est capable de prévoir avec précision qu'une image donnée appartient à une certaine classe. L'architecture la plus populaire utilisée pour la classification des images est le réseau de neurones convolutifs (CNN). 

On peut citer des architectures courantes  en  classification d’image  tel  que :  AlexNet (2012),  GoogleNet  (2014),  ou encore ResNet (2015). 

 ![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.004.png)

*Figure 4 : Architecture AlexNet (Alexnet,2012)*

- **Le tracking d’objet**  fait référence au processus de suivi d'un objet d'intérêt spécifique, ou de plusieurs objets, dans une scène donnée. Les méthodes de suivi d'objets peuvent être divisées en 2 catégories selon  le  modèle  d'observation  :  méthode  générative  (modèle génératif comme PCA) et méthode discriminative (distinction entre objet et arrière-plan). Le réseau profond le plus populaire pour le suivi des tâches à l'aide des encodeurs automatiques empilés (SAE) est Deep Learning Tracker. On peut citer 2 algorithmes de suivi basés  sur  CNN  et  régions  d’intérêts  (ROI),  le  suivi  de  réseau entièrement convolutif (FCNT) et le CNN multi-domaine (MD Net).
- **La segmentation sémantique**  repose sur la division d’images entières en groupes de pixels qui peuvent ensuite être étiquetés et classés. Ici, on tente de comprendre sémantiquement le rôle de chaque  pixel dans l'image. Par  conséquent,  contrairement  à  la classification, nous avons besoin de prédictions denses au niveau des pixels de nos modèles. La solution est les réseaux entièrement convolutifs  (FCN)  de  l'UC  Berkeley,  qui  ont  popularisé  les architectures CNN de bout en bout pour des prédictions denses sans couches entièrement connectées.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.005.jpeg)

*Figure 5 : Segmentation sémantique d’une image (Pulkit Sharma, 2019)*

- **La segmentation d’instances**  consiste à segmenter différentes instances de classes. Nous observons des images avec de multiples objets qui se chevauchent et des arrière-plans différents, et non seulement nous classons ces différents objets, mais nous identifions

également leurs limites, leurs différences et leurs relations les uns avec les autres ! Ce problème de segmentation d'instance est exploré chez Facebook AI en utilisant une architecture connue sous le nom de Mask R-CNN.

- **L’estimation  de  pose** est  une méthode utilisée pour déterminer où se trouvent  les  articulations  d'une personne ou d'un objet dans une image et  ce  que  le  placement  de  ces articulations indique. Il peut être utilisé avec  des  images  2D  et  3D. L'architecture  principale  utilisée  pour l'estimation de la pose est PoseNet. 

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.006.png)

*Figure 6 : Estimation de pose (Sumedh Datar, 2023)*

- **La  détection  d’objets**  utilise  la  classification  d'images  pour identifier une certaine classe d'images, puis détecter et localiser leur emplacement dans une boîte englobante (bounding box). On peut distinguer  deux  éléments  principaux  constituant  la  détection d’objets : 
- Le positionnement de chaque objet dans l’image dans des bounding boxes
- L’affiliation de labels à chaque objet afin d’identifier sa classe au fil du flux d’images

Il existe deux types courants de détection d'objets effectués via des techniques de vision par ordinateur :

- Détection d'objets en deux étapes  - la première étape nécessite un réseau de proposition de région (RPN), fournissant un certain nombre  de  régions  candidates  pouvant  contenir  des  objets importants.  La  deuxième  étape  consiste  à  transmettre  les propositions  de  régions  à  une  architecture  de  classification neuronale,  généralement  un  algorithme  de  regroupement hiérarchique basé sur R-CNN, ou un regroupement de régions d'intérêt  (ROI)  dans  Fast  R-CNN.  Ces  approches  sont  assez précises, mais peuvent être très lentes. Les méthodes à deux étapes donnent la priorité à la précision de la détection.
- Détection d'objets en une étape   - avec le besoin de détection d'objets en temps réel, des architectures de détection d'objets en une étape ont émergé, telles que YOLO, SSD et RetinaNet. Celles- ci  combinent  l'étape  de  détection  et  de  classification,  en régressant les prédictions de la boîte englobante. Chaque boîte englobante  est  représentée  avec  seulement  quelques

coordonnées,  ce  qui  facilite  la  combinaison  de  l'étape  de détection  et  de  classification  et  accélère  le  traitement.  Les méthodes à une étape donnent la priorité à la vitesse d'inférence.

3) **Synthèse des techniques**

**La reconnaissance d’action ou HAR (Human Activity Recognition)** est un domaine de recherche en intelligence artificielle qui vise à détecter et classifier les actions humaines à partir de données d'images et/ou de vidéos.  Cette  tâche  peut-être  complexe  en  raison  de  la  variabilité interpersonnelle,  des  différences  de  perspectives,  des  conditions d'éclairage, de la variabilité de la vitesse de l'action et de l'occlusion. Cependant,  ces  défis  ont  stimulé  le  développement  de  nombreuses approches pour améliorer la précision et la robustesse des détecteurs d'actions, chacune ayant ses avantages et limites.

En  intelligence  artificielle,  on  parle  de  reconnaissance  d’action  pour désigner le processus d’interprétation des mouvements humain par des technologies de vision par ordinateur. Le mouvement humain peut être interprété comme des activités, des gestes ou des comportements qui sont enregistrés par des capteurs. Les données de mouvement sont ensuite  traduites  en  commandes  d’action  pour  que  les  ordinateurs puissent les comprendre et les reconnaitre.

**Historique :**

Techniquement parlant, la reconnaissance de l’activité humaine par la vision par ordinateur demeure un domaine difficile. La complexité de la détection des activités et le nombre de sujets présents dans l’analyse sont les principaux enjeux.

Premièrement, la complexité de l’estimation de la pose humaine a été abordée  au  moyen  de  techniques  traditionnelles.  Ces  dernières comprenaient des modèles de support vector machines (SVM), arbres de décision ou k-nearest neighbour (k-NN). En effet, les méthodes utilisées avant l’apprentissage en profondeur étaient des approches artisanales basés  sur  l’extraction  des  caractéristiques,  classification  des caractéristiques et représentation des caractéristiques. Des extracteurs de caractéristiques ‘handmade’ tels que HOG (Dalal et al. 2015), HOF (Laptev et al. 2008), SIFT (Lowe 2004), SURF (Bay, Tuytelaars et Van Gool 2006), … sont utilisés pour extraire les features à la main. Cependant, ils étaient incapables de saisir des mouvements complexes avec une séquence de micro-activités et souvent biaisés par la base de données.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.007.jpeg)

*Figure 7 : Human Activity Recognition (HAR) exemples (Shreyas Tripathi, 2019)*

C’est  pourquoi  le  deep  learning  est  devenue  une  technologie prédominante pour relever le défi de la reconnaissance d’action. En 2011, Kwapisz, Weiss & Moore observait que le multi layer perceptron (MLP) donnait de meilleurs résultats que les arbres de décision ou la régression logistique pour le HAR. Ainsi, des approches basées sur l’apprentissage profond pour le HAR ont vu le jour à partir de 2014 présentant de nombreux avantages. Contrairement aux approches artisanales basées sur les features, une méthode HAR basée sur l'apprentissage en profondeur peut apprendre simultanément des features visuelles, des représentations de features et des classificateurs. 

En 2014, deux articles révolutionnaires importants ont donné le coup d'envoi à l'apprentissage en profondeur dans la reconnaissance vidéo. Large-scale Video Classification with Convolutional Neural Networks  par Karpathy  et.  al.  et  Two-Stream  Convolutional  Networks  for  Action Recognition in Videos  par Simonyan et Zisserman ont donné lieu à la popularité des réseaux à flux unique et à deux flux dans la reconnaissance d'action.

**Limites du projet :**

Il est important de souligner qu’une des catégories de captation pour le HAR  repose  sur  l’acquisition  de  données  inertielle  et  se  trouva particulièrement performant avec des réseaux de neurones convolutifs (CNN) comme l’expliqua Imran & Raman en 2020. Ainsi, on peut distinguer qu’une première approche sortant des limites de notre projet repose sur l’acquisition de données à base de capteurs inertielles (IMU) qui couplés

avec des algorithmes de HAR sont plutôt performant. Cependant, ces approches sont très invasives car elle demande la pose d’une multitude de capteurs. Ici, nous nous intéresserons seulement aux approches liées à la computer vision qui permettent une reconnaissance efficace en temps réel en étant seulement équipé d’une caméra. 

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.008.png)

*Figure 8 : Human activity recognition pipeline using IMU signals ( Wenjin Tao, 2021)*

**Deux modèles principaux en HAR : CNN et RNN**

Les architectures d'apprentissage en profondeur ont différentes variantes, mais les modèles les plus attrayants pour le HAR basés sur la vision sont **le réseau neuronal convolutif** (CNN) et  **le réseau neuronal récurrent** (RNN). Nous étudierons 5 architectures des plus performantes pour le HAR.

- Réseau CNN two-stream :

En effet, au cours des dernières années, les  **CNN** se sont avérés être l'un des modèles de traitement d'image les plus performants. Ces réseaux sont particulièrement bien adaptés à la reconnaissance de motifs dans des images et des séquences vidéo. Pour notre tâche précise, des réseaux multiflux ont vu le jour, ces modèles d’apprentissage en profondeur utilisent  des  flux  spatiaux  et  temporels  distincts  dans  l'architecture. Introduite par Peng et al. en 2016, on trouve les CNN à deux flux, où le premier flux est destiné aux features spatiales de la vidéo, tandis que le second flux se concentre sur les features temporelles. Le flux spatial reconnaît l'action à partir d'images fixes, et le flux temporel effectue une reconnaissance d'action sous la forme d'un flux optique dense. Enfin, ces deux flux sont combinés à l'aide de la fusion tardive, en une seule représentation  de  caractéristiques  globale,  qui  est  utilisée  pour  la classification des activités. Cette approche a permis aux chercheurs de créer  des  caractéristiques  spatio-temporelles  et  de  modéliser  les dépendances temporelles des actions à long terme.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.010.jpeg)

*Figure 9 : Exemple d'architecture CNN multi-flux ( Peng  et al., 2016)*

- Réseau 3D-CNN (C3D)  : 

On constate également l’apparition des réseaux de neurones convolutifs 3D (C3D), basés sur les réseaux de neurones convolutifs classiques 2D, mais qui prennent également en compte la dimension temporelle des séquences vidéo en considérant les séquences comme des volumes 3D. Les ConvNets 3D sont apparus dans le document de recherche de 2015 Learning Spatiotemporal Features with 3D Convolutional Networks par Du Tran et. Al.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.011.jpeg)  

Figure 10 : Architecture CNN 3D pourclassification (Koushik Nagasubramanian  et al., 2018)

Les couches de convolution 3D effectuent des opérations de convolution sur des fenêtres tridimensionnelles de la séquence vidéo, en capturant les caractéristiques spatiales et temporelles de la vidéo à l’aide de filtres 3D. Cette architecture est puissante dans la mesure où de nombreuses vidéos peuvent être traitées en temps réel.

- Réseau LSTM + CNN :

L'architecture LSTM a été popularisée par l'article de  2014 Long-term Recurrent Convolutional Networks for Visual Recognition and Description par Donahue et. al . L'architecture inspiré des  **RNN** est connue sous le nom  de  LRCN.  L'architecture  LRCN  consiste  à  utiliser  un  réseau  de neurones convolutifs pour extraire des caractéristiques à partir de chaque image de la séquence vidéo, puis en ajouter une couche récurrente en

utilisant un LSTM pour modéliser la dépendance temporelle entre ces caractéristiques.  Les  caractéristiques  extraites  à  partir  du  CNN  sont transformées en une séquence de vecteurs, qui sont ensuite traités par le LSTM pour modéliser les dépendances temporelles à long terme.

- Réseau Two-Stream Inflated 3D ConvNet (I3D)  : 

En 2017,  Quo Vadis, Action Recognition ? A New Model and the Kinetics Dataset, par Joao Carreira et Andrew Zisserman fait avancer C3D en le fusionnant avec les apprentissages des réseaux à deux flux, pour donner le réseau I3D.

Leur approche commence par une architecture 2D et gonfle tous les filtres et les noyaux de pooling, c’est la méthode appelée « inflation ». En les gonflant,  ils  ajoutent  une  dimension  supplémentaire  à  prendre  en

considération, qui dans notre cas est le temps. Ainsi, les noyaux dans I3D ne  sont  pas  symétriques  à  cause  de  la  dimension  temporelle supplémentaire.

L'idée principale de I3D est d'utiliser une architecture de CNN 2D pré- entraînée pour initialiser les poids des filtres de convolution dans une architecture de CNN 3D. Les filtres de convolution pré-entraînés sur les images fixes sont ensuite étendus à des filtres de convolution 3D en ajoutant une dimension temporelle.

Ainsi,  les  caractéristiques  spatiales  et  temporelles  de  la  vidéo  sont capturées à chaque couche du réseau de neurones convolutifs 3D, en utilisant des filtres de convolution qui prennent en compte à la fois la position  spatiale  et  la  position  temporelle  de  chaque  pixel  dans  la séquence vidéo. 

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.012.png)

*Figure 11 : Architecture I3D (Madeleine Schiappa, 2020)*

Le réseau I3D témoigne alors de 3 composantes passant par :
- L’inflation 2D des ConvNets en 3D : N x N to N x N x N 
- L’initialisation des filtres 3D à partir des poids des filtres 2D
- L’utilisation de deux streams I3D, l’un pour le flux RGB et l’autre qui optimise et smooth l’information

Cette approche témoigne de résultats rapides et efficaces en situation de transfert learning et en font ainsi une architecture de choix pour la reconnaissance HAR en temps réel. 

- Réseau de convolution séparés par canaux (CSN)  :

Plus récemment, en juin 2019, Du Tran et. Al. proposent des réseaux de convolution séparés par canaux (CSN) pour la tâche de reconnaissance d'action dans  Video Classification with Channel-Separated Convolutional Networks.  CSN est basé sur l'idée de séparer les filtres de convolution pour chaque canal de couleur de l'image d'entrée, puis de les réassembler dans une étape ultérieure.

Le premier bloc de convolution traite les canaux rouges et verts de l'image d'entrée, tandis que le second traite les canaux verts et bleus. Les sorties des deux blocs de convolution sont fusionnées en utilisant une couche de concaténation. L'architecture de CSN permet de traiter chaque canal de couleur de manière indépendante, ce qui améliore la robustesse du réseau de neurones aux changements de couleur et de luminosité de l'image d'entrée. De plus, en séparant les canaux de couleur, le réseau de neurones peut capturer des caractéristiques spatiales et temporelles plus riches.

Fondamentalement,  les  convolutions  de  groupe  introduisent  une régularisation et moins de calculs en n'étant pas entièrement connectées.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.013.jpeg)

*Figure 12 : Architectures significatives en HAR (Joao Carreira, Andrew Zisserman,2018)*

4) **Analyse critique**

Dans cette partie, nous nous pencherons sur l’analyse critique des techniques existantes afin d’éclairer nos choix sur la solution la plus adéquate pour notre problématique.

**Etude qualitative :**

- Réseau CNN two-stream :

Les avantages des réseaux de neurones convolutifs à deux flux sont la capacité à capturer les caractéristiques spatiales et temporelles de la vidéo à différents niveaux d'abstraction, la capacité à traiter des vidéos de longue durée en utilisant des flux de données de durées différentes, et la capacité à capturer les relations entre les objets dans la vidéo en utilisant des flux de données à plusieurs résolutions spatiales. Cette technique présente des performances élevés pour le HAR.

Cependant, ils ont également des inconvénients, tels que le grand nombre de paramètres à entraîner, pouvant entrainer un overfitting si les données d'entraînement ne sont pas suffisantes, et présentant un temps de calcul élevé pour l'entraînement et la prédiction, en raison de la taille importante du modèle, qui rende leur utilisation temps réel compliqué.

- Réseau 3D-CNN (C3D)  : 

Les  réseaux  C3D  permette  également  de  capturer  les  informations spatiales  et  temporelles  de  vidéo,  mais  en  utilisant  cette  fois  des convolutions 3D. Ils présentent également des performances élevées.

Cependant, les C3D ont également des inconvénients, tels que le grand nombre de paramètres à entraîner, pouvant entrainer un overfitting. De plus, l’architecture plus complexe que les réseaux de convolutions 2D, rendent l'entraînement et le déploiement plus difficiles.

- Réseau LSTM + CNN  : 

La force du réseau LRCN est qu'il peut gérer des séquences de différentes longueurs. Il peut également être adapté à d'autres tâches vidéo telles que le sous-titrage d'images et la description vidéo. Ce réseau présente l’avantage d’être entrainable end-to-end, c’est-à-dire sans introduction de décisions autres que celles du réseau, et capables de fonctionner en temps réel. Il capture les informations temporelles et spatiales grâce à la combinaison des deux réseaux.

Cependant ce réseau est difficile à entrainer dû à la complexité de l'architecture et le grand nombre de paramètres. Il peut ainsi nécessiter plus de temps d’entrainement. Enfin, il ne capture que les variations de mouvements de hauts niveaux. 

- Réseau Two-Stream Inflated 3D ConvNet (I3D)  : 

La force de I3D est sa  **combinaison**  des méthodes two-stream et C3D. Il utilise les convolutions 3D pour capturer les informations spatiales et temporelles  dans  les  séquences  vidéo  mais  son  architecture  est relativement plus simple par rapport à d’autres architectures 3D-CNN et présente également des performances élevées.

I3D a été évalué sur plusieurs ensembles de données de référence pour la reconnaissance  d'activités  humaines  en  vision  par  ordinateur  et  a démontré  des  performances  élevées,  dépassant  souvent  les  autres architectures de pointe. En outre, l'utilisation de l'entraînement préalable des CNN 2D pour initialiser les poids des filtres de convolution 3D a considérablement réduit le temps nécessaire pour entraîner le réseau de neurones. 

Cependant, I3D nécessite un grand nombre de paramètres et des données volumineuses part l’utilisation de la méthode two-stream, ce qui peut rendre  l'entraînement  et  le  déploiement  plus  difficiles.  Cette problématique peut ainsi être évité grâce au transfert learning.

- Réseau de convolution séparés par canaux (CSN)  :

La force du réseau CSN est son utilisation des convolutions séparées par canaux pour réduire la complexité des calculs, rendant l'entraînement et le  déploiement  plus  efficaces.  En  outre,  CSN  est  une  architecture relativement  simple,  qui  utilise  moins  de  paramètres  que  d'autres architectures de pointe, ce qui facilite l'entraînement et le déploiement.

En  revanche,  le  réseau  peut  ne  pas  capturer  autant  d'informations spatiales et temporelles que d'autres approches, et potentiellement être moins efficace qu’une autre architecture pour le même entraînement.

**Etude des performances :**

Voici  une  étude  comparative  des  performances  en  fonction  des architectures sur la dataset de référence UCF101 : 



|Architecture|Dataset|Pré- entrainement|<p>Taux de précision</p><p>(en %)</p>|
| - | - | :- | - |
|CNN Two-stream|UCF101|-|94.5 %|
|C3D|UCF101|-|85.5 %|
|LSTM+CNN|UCF101|-|86.4 %|
|**I3D**|UCF101|Kinetics|97.9 %|
|CSN|UCF101|Kinetics|90.4 %|

*Figure 13 : Comparaison des performances des architectures HAR*

En général, les architectures basées sur des convolutions 3D ont tendance à  avoir  des  performances  plus  élevées,  mais  sont  également  plus complexes et nécessitent plus de temps d'entraînement.

En  revanche,  l’utilisation  du  transfert  learning  avec  un  modèle  pré- entrainé permet de réduire ce temps d’entrainement et améliorer la performance du modèle plus rapidement.

Ainsi,  d’après  notre  étude,  nous  allons  utiliser  l’architecture  I3D  en transfert learning avec un pré-entrainement sur la dataset Kinetics-400 car ce réseau présente les meilleures performances, permet un apprentissage plus rapide et une vitesse d’inférence permettant une reconnaissance temps réel grâce à l’utilisation d’un flux two-stream.

**Utilisation de la plateforme open source MMAction2 :**

MMAction2 est une plateforme open source pour la reconnaissance d'actions  basée  sur  PyTorch.  Elle  propose  des  implémentations  de plusieurs architectures pour la reconnaissance d'activités humaines.

Présenté dans un article de recherche intitulé  "MMAction2: Towards Good Practices for Large-Scale Video Action Recognition" en 2020. MMAction2 permet aux utilisateurs de facilement entraîner et tester des modèles de

reconnaissance  d'activités  humaines  sur  plusieurs  jeux  de  données standard, tels que Kinetics. Cette plateforme servira ainsi à l’entraînement dans notre projet.

**Kinetics Dataset :**

La dataset Kinetics-400 consiste en :

- 400 classes HAR
- Au moins 400 clips vidéo par classe (téléchargés via YouTube)
- Un total de 300,000 vidéos

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.014.png)

*Figure 14 : Kinetics-400 dataset (Kay et al., 2017)*

**Mesure de la performance / métrique d’évaluation :**

La  performance  des  algorithmes  de  reconnaissance  d'activités humaines (HAR) est généralement mesurée par la  mAP  (mean Average Precision ).  Le  calcul  de **mAP**  nécessite **Precision** , **Recall** , **Precision Recall Curve** et **AP.** 

La  précision  est  la  proportion  d’exemples classifiés correctement parmi les exemples classifiés positifs.

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.015.png)

Le  rappel  est  la  proportion  d’exemples classifiés  correctement  parmi  les  exemples vraiment positifs 

![](./figure/Aspose.Words.179058ac-2aa6-4e4a-b6ed-d1b7e3c6fecb.016.png)

Ensuite,  on  trouve  la  précision  moyenne  (AP)  en  traçant  la  courbe Précision-Rappel, puis en appliquant une méthode d’interpolation.

Enfin, en calculant AP pour chaque classe, on peut isoler la  **mAP**  et déterminer efficacement la performance d’un modèle. En temps réel, le nombre de FPS (frame per second) influencera également la performance du modèle selon ses critères.

## Conclusion

Nous avons appris que l'apprentissage en profondeur a révolutionné la façon dont nous traitons les vidéos pour la reconnaissance d’action. Ainsi,  nous  avons  pu  découvrir  l’ensemble  des  méthodes  et  des techniques utilisés en vision par ordinateur associé à l’apprentissage profond. Nous avons pu fixer les tâches et les limites de notre projet de **reconnaissance d’action** . 

Notre documentation nous a permis de découvrir les méthodes courantes en HAR et d’identifier l’architecture  **I3D**  la plus pertinente pour notre application  en  temps  réel.  Enfin,  nous  avons  analyser  en  détail  les modèles et comparer leurs performances.

Donc, pour la suite du projet, une bonne recommandation serait d’utiliser cette architecture I3D en transfert learning avec un pré-entrainement sur la  dataset  Kinetics-400.  On  pourra  ainsi  apprécier  la  vitesse d’apprentissage sur notre dataset. 

En effet, actuellement dans une phase d’augmentation de la dataset, mon enjeu  est  d’avoir  plus  d’échantillons  et  qu’ils  soient  plus  variés. L’élaboration de cette dataset homemade représente ainsi un défi du projet, et on peut alors se demander dans quelles mesures sera-t-elle efficace pour notre tâche HAR.  Enfin, les tests dans le studio de CREO représentent également un enjeu proche dans le projet et nécessitent un entrainement efficace du modèle. 


## References / Références

- [@ian_goodfellow__yoshua_bengio__aaron_courville_deep_2016]

- [@aubert_quest-ce_2020]

- [@noauthor_quest-ce_nodate]

- [@noauthor_mean_2022]

- [@sharma_review_2022]

- [@schiappa_understanding_2020]

- [@le_5_2021]

- [@carreira_quo_2018]

- [@zharovskikh_human_2022]

- [@brownlee_deep_2018]

- [@marco_pedersoli_cours_nodate]

- [@noauthor_classification_nodate]

- [@ghewari_action_2017]

- [@noauthor_deep_nodate]

- [@sharma_deep_2020]

- [@noauthor_cours_nodate]

- [@li_survey_2022]

### At SAT metalab

Tools from SAT metalab to review in this report may include:

- LivePose

## Acknowledgements / Remerciements

