## Feedback

Note that you may make your [issue confidential](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html): your issue will be hidden from public view (useful if your internship repository is or when it becomes public), but your issue [can still be accessed by repository members with reporter and higher permission levels](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#permissions-and-access-to-confidential-issues).

### Participants

Please list usernames: yours and of people you would like to give feedback to: 

* ...

### Comments

Please write your feedback under the following subsections:

#### What works

...

#### What could be improved

...

#### Other comments

...

/label ~feedback